import picamera
import time
from datetime import datetime
import RPi.GPIO as GPIO
import threading
import subprocess
#from subprocess import call
from os.path import isfile, join, splitext, basename
from os import remove, listdir

gpio_port = 27
upload_dir = "/drive/piphotos"


def upload(filename):
    try:
        result = subprocess.call(["drive", "push", "-no-prompt", "-quiet", filename])
        print('Photo uploaded ' + filename)
        if result == 0:
            remove(filename)
            print("File removed")
    except:
        print("Exception in execution")

def upload_async(filename):
    threading.Thread(target=upload, kwargs={"filename": filename}).start()



def cleanup_remaining_files():
    print("Start cleanup")
    remaining_files = [f for f in listdir(upload_dir) if isfile(join(upload_dir, f))]

    for f in remaining_files:
        filename = join(upload_dir, f)
        if is_file_older_2min(filename):
            upload_async(filename)



def is_file_older_2min(filename):
    name = splitext(basename(filename))[0]

    datetime_object = datetime.strptime(name, "%Y%m%d-%H%M%S")
    diff =  datetime.now() - datetime_object

    if diff.seconds > 120:
        return True
    else:
        return False


##################################################################
GPIO.setmode(GPIO.BCM)


GPIO.setup(gpio_port, GPIO.IN, pull_up_down=GPIO.PUD_UP)
camera = picamera.PiCamera()

cleanup_remaining_files()
threading.Timer(60, cleanup_remaining_files).start()

while True:
    input_state = GPIO.input(gpio_port)
    if input_state == False:
        print('Button Pressed')
        timestr = time.strftime("%Y%m%d-%H%M%S")
        filename = join(upload_dir, timestr + '.jpg')
        camera.capture(filename)
        print('Photo taken ' + filename)
        upload_async(filename)
        time.sleep(0.4)


